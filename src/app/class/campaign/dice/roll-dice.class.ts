export class RollDice {
  nomePlayer: String = '' ;
  tipo: String = '';
  nomePericia: String = '';
  formula: String = '';
  resultadoFinal: number = 0;
}
